#  基于NPOI的Excel工具

### 介绍

该工具类是基于`NPOI`进行实现的。主要为刚入职的或有相关需求的小伙伴提供解决方案及思路。在快速的实现功能的基础上，对`NPOI`的使用有基本的认识。本类库使用`.NET 5`框架实现，但是在NPOI的使用上与`.NET Framework`基本一致，所以无需担心不同框架下语法的差异性（当然不同框架下引用的`NPOI`库肯定也是不同的）。

> NPOI：POI的.NET版，POI是一套用Java写成的库,能够帮助开发者在没有安装微软Office的情况下也可以读写相关文件。


### 使用说明

#### 按模板导出

主要用于固定模板的填充导出。其中包括对模板的复制、卡片类型数据填充、表格填充。本类库通过定义标签，并根据标签的含义进行相关的操作，所以按模板导出的第一步都是获取Excel模板中的标签，再解析请求条件及标签，进行循环复制及数据填充，最后导出新的Excel。

**主要调用方法：**

```csharp
	public class ExcelToolsOpr
    {
		/// <summary>
        /// 根据模板导出Excel
        /// </summary>
        /// <param name="templatePath">模板位置</param>
        /// <param name="exportPath">导出文件的文件夹路径</param>
        /// <param name="fileName">文件名(不需要后缀)</param>
        /// <param name="queryCon">相关条件</param>
        public static void ExportExcelByTemplate(string templatePath, string exportPath, string fileName, QueryCon queryCon){
            //....
        }
        
        //...
    }
```




 **请求对象类展示：** 
```C#
    //请求对象
    public class QueryCon
    {
        //循环复制的条件
        public List<LoopCon> LoopCons { get; set; }
		
        //请求数据
        public DataSet DataSet { get; set; }
    }

    public class LoopCon
    {
        /// <summary>
        /// 循环关键字
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 间隔行数
        /// </summary>
        public int SpaceRowCnt { get; set; }

        /// <summary>
        /// 循环测试
        /// </summary>
        public int LoopCount { get; set; }
    }
```

> 注意：
>
> 模板导出对模板有基本的要求，详见各类型操作说明。
>
> 不用考虑Excel文件版本的问题，NPOI有相关方法帮我们做好处理生成对应类型的对象，它们都实现了相同的接口。

##### 循环

###### 实现逻辑

通过约定模板中的标签及其含义，根据请求条件，对每个sheet页中标记范围内容进行循环复制。考虑到可能有不同块需要复制的情况，所以标签中加入了关键字段落，在请求对象中可以进行区分。

###### 使用说明

在页的首行必须插入标签（单元格中填写）“**#loopTagColumn**”，用于定位对应标签列，方便寻找后续循环复制标签的位置，而这些标签都是用于定位需要复制块的行范围。

其中卡片类型的标签为“**#loop_KeyStr_begin**”及“**#loop_KeyStr_end**”。

> 井号作为标签的标准起始符，后面的标签用下划线分为三段，分别代表填充类型（循环复制）、对应请求对象中Key字符、起终点位置。

##### 卡片与表格

###### 实现逻辑

###### 使用说明

在页的首行必须插入标签（单元格中填写）“**#baseTagColumn**”，用于定位对应标签列，方便寻找后续基本标签的位置，而这些标签都是用于定位填充卡片或表格的行范围。

其中卡片类型的标签为“**#card_DataTableName_begin**”及“**#card_DataTableName_end**”。表格类型的标签为“**#table_DataTableName**”。

> 井号作为标签的标准起始符。卡片后面的标签用下划线分为三段，分别代表填充类型（卡片）、对应填充数据DataTable表名、起终点位置；表格后面的标签用下划线划分为两段，分别代表填充类型（表格），对应填充数据DataTable表名。

##### 示例

详见项目中单元测试工程`ExcelTools.Test`的`ExportTemplateTest.cs`。包括了模板复制、卡片与表格的填充的测试样例。
![示例Excel](https://images.gitee.com/uploads/images/2021/0613/230248_4f92b6a1_8396135.png "示例模板")

#### 无模板导出

对于简单的DateTable导出Excel，在无复杂要求时，制作固定模板会减少相关代码的灵活性，增加工作压力。所以可以调用无模板导出方法实现导出的过程，无需关心模板。

**主要调用方法：**

```csharp
	public class ExcelToolsOpr
    {
        /// <summary>
        /// 根据DataSet导出Excel
        /// </summary>
        /// <typeparam name="T">HSSFWorkbook:是操作Excel2003以前（包括2003）的版本，扩展名是.xls；XSSFWorkbook、SXSSFWorkbook:是操作Excel2007后的版本，扩展名是.xlsx；</typeparam>
        /// <param name="ds">DataSet</param>
        /// <param name="exportPath">导出文件的文件夹路径</param>
        /// <param name="fileName">文件名(不需要后缀)</param>
        /// <param name="titleFlag">是否使用TableName作为标题显示</param>
        public static void DataSetToExcel<T>(DataSet ds, string exportPath, string fileName,
            bool titleFlag = false, CellStyle titleStyle = null, CellStyle colStyle = null, CellStyle contentStyle = null) where T : IWorkbook, new()
        {
            //....
        }
        
        //...
    }
```

#### 读取Excel（待补充）

