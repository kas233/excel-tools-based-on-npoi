using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExcelTools;
using ExcelTools.BaseModels;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace ExcelTools.Test
{
    [TestClass]
    public class ExportTemplateTest
    {
        [TestMethod]
        public void TestMethod()
        {
            var templPath = Environment.CurrentDirectory + @"\resource\test.xlsx";
            var exportPath = Environment.CurrentDirectory + @"\resource";
            var fileName = "testExport";
            var loopConLst = new List<LoopCon>();
            loopConLst.Add(new LoopCon()
            {
                Key = "a",
                SpaceRowCnt = 1,
                LoopCount = 3
            });
            var ds = new DataSet();
            ds.Tables.Add(GetTestDataTable1());
            ds.Tables.Add(GetTestDataTable2());
            ds.Tables.Add(GetTestDataTable3());

            var queryCon = new QueryCon()
            {
                LoopCons = loopConLst,
                DataSet = ds
            };

            ExcelToolsOpr.ExportExcelByTemplate(templPath, exportPath, fileName, queryCon);
        }

        private DataTable GetTestDataTable1()
        {
            var dt = new DataTable();
            dt.TableName = "Test";

            var strLst = new List<string>();
            for (int i = 1; i < 10; i++)
            {
                strLst.Add("test" + i.ToString());
            }

            dt.Columns.AddRange(strLst.Select(x => new DataColumn(x, typeof(double))).ToArray());

            for (int i = 0; i < 3; i++)
            {
                var dr = dt.NewRow();
                for (int j = 0; j < 9; j++)
                {
                    dr[j] = new Random().NextDouble();
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }

        private DataTable GetTestDataTable2()
        {
            var dt = new DataTable();
            dt.TableName = "TestCard-0";

            dt.Columns.AddRange(new string[] { "name", "sex", "age", "occupation", "address", "tel", "remark" }.Select(x => new DataColumn(x)).ToArray());

            var dr = dt.NewRow();
            dr[0] = "小明";
            dr[1] = "男";
            dr[2] = "23";
            dr[3] = "码农";
            dr[4] = "神州";
            dr[5] = "135xxxxxxxx";
            dr[6] = "这是一条有意思的备注。";

            dt.Rows.Add(dr);

            return dt;
        }
        private DataTable GetTestDataTable3()
        {
            var dt = new DataTable();
            dt.TableName = "TestCard-1";

            dt.Columns.AddRange(new string[] { "name", "sex", "age", "occupation", "address", "tel", "remark" }.Select(x => new DataColumn(x)).ToArray());

            var dr = dt.NewRow();
            dr[0] = "小王";
            dr[1] = "男";
            dr[2] = "22";
            dr[3] = "也是码农";
            dr[4] = "神州";
            dr[5] = "135xxxxxxxx";
            dr[6] = "这是一条有意思的备注。";

            dt.Rows.Add(dr);

            return dt;
        }
    }
}
