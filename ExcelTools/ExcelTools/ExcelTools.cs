﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ExcelTools.BaseModels;
using NPOI.SS.UserModel;

namespace ExcelTools
{
    public class ExcelToolsOpr
    {
        /// <summary>
        /// 根据模板导出Excel
        /// </summary>
        /// <param name="templatePath">模板位置</param>
        /// <param name="exportPath">导出文件的文件夹路径</param>
        /// <param name="fileName">文件名(不需要后缀)</param>
        /// <param name="queryCon">相关条件</param>
        public static void ExportExcelByTemplate(string templatePath, string exportPath, string fileName, QueryCon queryCon)
        {
            var wb = BaseOperation.GetWorkbook(templatePath);

            var baseTagInfos = wb.FindTags(out List<LoopTagInfo> loopTagInfos);

            if (loopTagInfos != null && loopTagInfos.Count > 0)
            {
                loopTagInfos.ForEach(x =>
                {
                    var loopCon = queryCon.LoopCons.FirstOrDefault(y => y.Key == x.Key);
                    if (loopCon == null)
                    {
                        x.SpaceRowCnt = 0;
                        x.Count = 1;
                    }
                    else
                    {
                        x.SpaceRowCnt = loopCon.SpaceRowCnt;
                        x.Count = loopCon.LoopCount;
                    }
                });
                wb.CopyRange(loopTagInfos, baseTagInfos);
            }

            wb.FillExcel(baseTagInfos, queryCon.DataSet);

            wb.Save(exportPath, fileName);
        }

        /// <summary>
        /// 根据DataSet导出Excel
        /// </summary>
        /// <typeparam name="T">HSSFWorkbook:是操作Excel2003以前（包括2003）的版本，扩展名是.xls；XSSFWorkbook、SXSSFWorkbook:是操作Excel2007后的版本，扩展名是.xlsx；</typeparam>
        /// <param name="ds">DataSet</param>
        /// <param name="exportPath">导出文件的文件夹路径</param>
        /// <param name="fileName">文件名(不需要后缀)</param>
        /// <param name="titleFlag">是否使用TableName作为标题显示</param>
        public static void DataSetToExcel<T>(DataSet ds, string exportPath, string fileName,
            bool titleFlag = false, CellStyle titleStyle = null, CellStyle colStyle = null, CellStyle contentStyle = null) where T : IWorkbook, new()
        {
            var workbook = BaseOperation.CreateWorkbook<T>();

            Dictionary<string, ICellStyle> titleStyleDic = titleStyle == null ? workbook.CreateICellStyle() : workbook.CreateICellStyle(titleStyle);
            Dictionary<string, ICellStyle> colStyleDic = colStyle == null ? workbook.CreateICellStyle() : workbook.CreateICellStyle(titleStyle);
            Dictionary<string, ICellStyle> contentStyleDic = contentStyle == null ? workbook.CreateICellStyle() : workbook.CreateICellStyle(titleStyle);

            for (int i = 0; i < ds.Tables.Count; i++)
            {
                DataTable dt = ds.Tables[i];
                ISheet tSheet;
                tSheet = workbook.CreateSheet();
                if (!string.IsNullOrWhiteSpace(dt.TableName))
                {
                    workbook.SetSheetName(i, dt.TableName);
                }

                ExportWithOutTemplate.FillSheet(titleStyleDic, colStyleDic, contentStyleDic, tSheet, dt, titleFlag: titleFlag);
            }

            workbook.Save(exportPath, fileName);
        }
    }
}
