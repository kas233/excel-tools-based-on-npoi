﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelTools.BaseModels
{
    public class TagInfo
    {
        public int SheetIndex { get; set; }
    }

    public class BaseTagInfo : TagInfo
    {

        public int TagColumnIndex { get; set; }

        public TagType TagType { get; set; }

        public string DataTableName { get; set; }

        /// <summary>
        /// 标签的行范围，卡片有起止范围，表格仅有行索引
        /// </summary>
        public int[] RowIndexs { get; set; }
    }

    public class LoopTagInfo : TagInfo
    {

        public string Key { get; set; }

        public int StartRowIndex { get; set; }

        public int EndRowIndex { get; set; }

        public int SpaceRowCnt { get; set; }

        public int Count { get; set; }
    }

    /// <summary>
    /// 填充标签类型
    /// </summary>
    public enum TagType
    {
        Table = 1,
        Card = 2
    }
}
