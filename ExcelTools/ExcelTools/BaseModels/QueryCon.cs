﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelTools.BaseModels
{
    public class QueryCon
    {
        public List<LoopCon> LoopCons { get; set; }

        public DataSet DataSet { get; set; }
    }

    public class LoopCon
    {
        /// <summary>
        /// 循环关键字
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 间隔行数
        /// </summary>
        public int SpaceRowCnt { get; set; }

        /// <summary>
        /// 循环测试
        /// </summary>
        public int LoopCount { get; set; }
    }
}
