﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelTools.BaseModels
{
    /// <summary>
    /// 单元格样式，包括字体粗细、名称；边框类型，后续可扩展
    /// </summary>
    public class CellStyle
    {
        /// <summary>
        /// 样式覆盖行数，主要用于加边框时，单行与多行的区别
        /// </summary>
        public string RowCount { get; set; }

        /// <summary>
        /// 字体是否为粗体
        /// </summary>
        public bool FontIsBold { get; set; }

        /// <summary>
        /// 字体名称
        /// </summary>
        public string FontName { get; set; }


        /// <summary>
        /// 边框样式
        /// </summary>
        public BorderType BorderType { get; set; }
    }

    public enum BorderType
    {
        /// <summary>
        /// 无边框
        /// </summary>
        None = 1,

        /// <summary>
        /// 正常
        /// </summary>
        Common = 2,

        /// <summary>
        /// 外边边框加粗
        /// </summary>
        RowBorderBlod = 3,

        /// <summary>
        /// 单元格边框加粗
        /// </summary>
        CellBorderBlod = 4
    }
}
